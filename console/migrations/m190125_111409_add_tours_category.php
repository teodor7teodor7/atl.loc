<?php

use yii\db\Migration;

/**
 * Class m180124_111409_add_page_category
 */
class m190125_111409_add_tours_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%tour_category}}', [
        'id'                   => $this->primaryKey(),
        'title'                => $this->string(255)->notNull(),
        'url'                  => $this->string(255)->notNull(),
        'created_at'           => $this->integer()->notNull(),
        'updated_at'           => $this->integer()->notNull()
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%tour_category}}');
    }
}
