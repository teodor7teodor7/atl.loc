<?php

use yii\db\Migration;

/**
 * Class m180210_133055_add_excursion_type_excursion
 */
class m180210_133055_add_excursion_type_excursion extends Migration
{
    /**
     * @inheritdoc
    */
    public function safeUp()
    {
        $this->createTable('{{%excursion_type_excursion}}', [
            'excursion_id' => $this->integer()->notNull(),
            'type_excursion_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%excursion_type_excursion}}');
    }
}
