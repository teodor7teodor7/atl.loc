<?php

use yii\db\Migration;

/**
 * Class m171225_094004_add_excursions
 */
class m180210_094004_add_excursions extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%excursion}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
            'text'                 => $this->text(),
            'url'                  => $this->string(255)->notNull(),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);
    }


    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%excursion}}');
    }

}
