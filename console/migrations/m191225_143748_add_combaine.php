<?php

use yii\db\Migration;

/**
 * Class m171225_143748_add_type_cost
 */
class m191225_143748_add_combaine extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%combaine}}', [
            'id' => $this->primaryKey(),
            'excursion_id' => $this->integer()->notNull(),
            'city_switch' => $this->smallInteger()->defaultValue(0),
            'date_start' => $this->integer()->notNull(),
            'period_data' => $this->integer(),
            'period_type' => $this->integer(),
            'region_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

    $this->addForeignKey('fk-combaine-excursion_id', '{{%combaine}}', 'excursion_id', '{{%excursion}}', 'id', 'RESTRICT');
    $this->addForeignKey('fk-combaine-region_id', '{{%combaine}}', 'region_id', '{{%region}}', 'id', 'RESTRICT');

   }


    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%combaine}}');
    }
}
