<?php

use yii\db\Migration;

/**
 * Class m180215_090330_add_pickup_point
 */
class m180215_090330_add_pickup_point extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pickup_point}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'city_id' => $this->string(255)->notNull(),
            'child_switch' => $this->smallInteger()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%pickup_point}}');
    }

}
