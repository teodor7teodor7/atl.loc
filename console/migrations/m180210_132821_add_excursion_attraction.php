<?php

use yii\db\Migration;

/**
 * Class m180210_132821_add_excursion_attraction
 */
class m180210_132821_add_excursion_attraction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%excursion_attraction}}', [
            'excursion_id' => $this->integer()->notNull(),
            'attraction_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%excursion_attraction}}');
    }
}
