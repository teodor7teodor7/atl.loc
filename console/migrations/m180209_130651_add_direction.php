<?php

use yii\db\Migration;

/**
 * Class m180209_130651_add_direction
 */
class m180209_130651_add_direction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%direction}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%direction}}');
    }
}
