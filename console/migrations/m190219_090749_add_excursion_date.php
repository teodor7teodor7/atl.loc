<?php

use yii\db\Migration;

/**
 * Class m180219_090749_add_excursion_date
 */
class m190219_090749_add_excursion_date extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%excursion_date}}', [
            'id' => $this->primaryKey(),
            'data' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'combaine_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%excursion_date}}');
    }

}
