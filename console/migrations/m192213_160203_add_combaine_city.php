<?php

use yii\db\Migration;

/**
 * Class m180213_160203_add_excursion_city
 */
class m192213_160203_add_combaine_city extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%combaine_city}}', [
            'combaine_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
        ]);
    }


    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%combaine_city}}');
    }
}
