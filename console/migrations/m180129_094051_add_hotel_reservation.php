<?php

use yii\db\Migration;

/**
 * Class m180129_094051_add_hotel_reservation
 */
class m180129_094051_add_hotel_reservation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%hotel_reservation}}', [
        'id'                   => $this->primaryKey(),
        'type_customer'             => $this->string(255)->notNull(),
        'name_agent'           => $this->string(255),
        'name_agency'          => $this->string(255),
        'number_adults'        => $this->integer()->defaultValue(1),
        'number_children'      => $this->integer()->defaultValue(0),
        'date_arrival'         => $this->integer()->notNull(),
        'date_departure'       => $this->integer()->notNull(),
        'type_food'            => $this->integer()->notNull(),
        'childrens_age'        => $this->string(255),
        'email'                => $this->string(255)->notNull(),
        'phone'                => $this->string(255)->notNull(),
        'created_at'           => $this->integer()->notNull(),
        'updated_at'           => $this->integer()->notNull()
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%hotel_reservation}}');
    }

}
