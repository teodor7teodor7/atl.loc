<?php

use yii\db\Migration;

/**
 * Class m180124_121050_add_page
 */
class m180125_111410_add_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%page}}', [
            'id'                   => $this->primaryKey(),
            'title'                => $this->string(255)->notNull(),
            'text'                 => $this->text(),
            'category_id'          => $this->integer()->notNull(),
            'url'                  => $this->string(255)->notNull(),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-page-category_id', '{{%page}}', 'category_id', 'page_category', 'id', 'RESTRICT');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%page}}');
    }

}
