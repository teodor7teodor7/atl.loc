<?php

use yii\db\Migration;

/**
 * Class m180209_094803_add_attraction
 */
class m180209_094803_add_attraction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%attraction}}', [
            'id'                   => $this->primaryKey(),
            'title'                => $this->string(255)->notNull(),
            'text'                 => $this->text(),
            'url'                  => $this->string(255)->notNull(),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%attraction}}');
    }
}
