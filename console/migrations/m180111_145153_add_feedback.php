<?php

use yii\db\Migration;

/**
 * Class m180111_125153_add_feedback
 */
class m180111_145153_add_feedback extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%feedback}}', [
            'id'                   => $this->primaryKey(),
            'topic_id'             => $this->integer()->notNull(),
            'name'                 => $this->string(255)->notNull(),
            'email'                => $this->string(255)->notNull(),
            'text'                 => $this->text(),
            'city'                 => $this->string(255)->notNull(),
            'exursion'             => $this->string(255)->notNull(),
            'date'                 => $this->integer()->notNull(),
            'answer'               => $this->text(),
            'status'               => $this->boolean()->defaultValue(false),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-topic_feedback-topic_id', '{{%feedback}}', 'topic_id', 'topic_feedback', 'id', 'RESTRICT');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%feedback}}');
    }

}
