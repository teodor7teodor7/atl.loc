<?php

use yii\db\Migration;

/**
 * Class m180212_134206_add_price
 */
class m191226_134206_add_price extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%price}}', [
            'id' => $this->primaryKey(),
            'typecost_id' => $this->integer()->notNull(),
            'excursion_id' => $this->integer()->notNull(),
            'price' => $this->decimal(7,2)->notNull()->defaultValue(0.00),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%price}}');
    }

}
