<?php

use yii\db\Migration;

/**
 * Class m180124_111409_add_page_category
 */
class m180125_111409_add_page_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%page_category}}', [
        'id'                   => $this->primaryKey(),
        'title'                => $this->string(255)->notNull(),
        'created_at'           => $this->integer()->notNull(),
        'updated_at'           => $this->integer()->notNull()
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%page_category}}');
    }
}
