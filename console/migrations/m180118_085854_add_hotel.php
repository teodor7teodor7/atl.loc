<?php

use yii\db\Migration;

/**
 * Class m180118_085854_add_hotel
 */
class m180118_085854_add_hotel extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%hotel}}', [
            'id'                   => $this->primaryKey(),
            'name'                 => $this->string(255)->notNull(),
            'text'                 => $this->text(),
            'url'                  => $this->string(255)->notNull(),
            'reting_id'            => $this->integer()->notNull(),
            'city_id'              => $this->integer()->notNull(),
            'reting_type'          => $this->integer()->notNull(),
            'status'               => $this->boolean()->defaultValue(false),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-hotel_reting-reting_id', '{{%hotel}}', 'reting_id', 'hotel_reting', 'id', 'RESTRICT');
        $this->addForeignKey('fk-hotel_city-city_id', '{{%hotel}}', 'city_id', 'hotel_city', 'id', 'RESTRICT');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%hotel}}');
    }

}
