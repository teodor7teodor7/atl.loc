<?php

use yii\db\Migration;

/**
 * Class m180119_120249_add_image_manager
 */
class m180119_120249_add_image_manager extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%image_manager}}', [
        'id'                   => $this->primaryKey(),
        'name'                 => $this->string(255)->notNull(),
        'class'                => $this->string(255)->notNull(),
        'item_id'              => $this->integer()->notNull(),
            'sort'             => $this->integer(),
        'created_at'           => $this->integer()->notNull(),
        'updated_at'           => $this->integer()->notNull(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%image_manager}}');
    }

}
