<?php

use yii\db\Migration;

/**
 * Class m180118_085355_add_hotel_reting
 */
class m180118_085355_add_hotel_reting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%hotel_reting}}', [
            'id'                   => $this->primaryKey(),
            'title'                => $this->string(255)->notNull(),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%hotel_reting}}');
    }

}
