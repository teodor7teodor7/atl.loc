<?php

use yii\db\Migration;

/**
 * Class m180222_130650_add_time_point
 */
class m198222_130650_add_time_point extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%time_point}}', [
            'point_id' => $this->integer()->notNull(),
            'day' => $this->integer()->notNull(),
            'combaine_id' => $this->integer()->notNull(),
            'time' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%time_point}}');
    }

}
