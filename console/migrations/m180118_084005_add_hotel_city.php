<?php

use yii\db\Migration;

/**
 * Class m180118_090005_add_hotel_city
 */
class m180118_084005_add_hotel_city extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%hotel_city}}', [
            'id'                   => $this->primaryKey(),
            'name'                 => $this->string(255)->notNull(),
            'url'                  => $this->string(255)->notNull(),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%hotel_city}}');
    }
}
