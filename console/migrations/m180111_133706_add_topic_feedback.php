<?php

use yii\db\Migration;

/**
 * Class m180111_133706_add_topic_feedback
 */
class m180111_133706_add_topic_feedback extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%topic_feedback}}', [
            'id'                   => $this->primaryKey(),
            'title'                => $this->string(255)->notNull(),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%topic_feedback}}');
    }

}
