<?php

use yii\db\Migration;

/**
 * Class m180209_144606_add_type_excursion
 */
class m180209_144606_add_type_excursion extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%type_excursion}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%type_excursion}}');
    }

}
