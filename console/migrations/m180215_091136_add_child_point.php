<?php

use yii\db\Migration;

/**
 * Class m180215_091136_add_childe_point
 */
class m180215_091136_add_child_point extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%child_point}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'parent_id' => $this->integer(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%child_point}}');
    }
}
