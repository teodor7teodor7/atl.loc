<?php

use yii\db\Migration;

/**
 * Class m180210_132709_add_excursion_direction
 */
class m180210_132709_add_excursion_direction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%excursion_direction}}', [
            'excursion_id' => $this->integer()->notNull(),
            'direction_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%excursion_direction}}');
    }

}
