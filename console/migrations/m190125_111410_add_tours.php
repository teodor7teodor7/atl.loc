<?php

use yii\db\Migration;

/**
 * Class m180124_121050_add_page
 */
class m190125_111410_add_tours extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%tour}}', [
            'id'                   => $this->primaryKey(),
            'title'                => $this->string(255)->notNull(),
            'text'                 => $this->text(),
            'category_id'          => $this->integer()->notNull(),
            'url'                  => $this->string(255)->notNull(),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-tour-category_id', '{{%tour}}', 'category_id', 'tour_category', 'id', 'RESTRICT');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%tour}}');
    }

}
