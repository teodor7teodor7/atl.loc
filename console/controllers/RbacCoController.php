<?php
/**
 * Created by PhpStorm.
 * User: noname
 * Date: 05.02.2018
 * Time: 10:41
 */

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\User;

class RbacCoController extends Controller
{
    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        // Создадим роли
        $admin = $auth->createRole(User::TYPE_ADMIN);
        $manager = $auth->createRole(User::TYPE_MANAGER);
        $operator = $auth->createRole(User::TYPE_OPERATOR);
        $user = $auth->createRole(User::TYPE_USER);
        $agent = $auth->createRole(User::TYPE_AGENT);

        // запишем их в БД
        $auth->add($admin);
        $auth->add($manager);
        $auth->add($operator);
        $auth->add($user);
        $auth->add($agent);

        // Создаем разрешения. Например, просмотр админки
        $viewDashboardIndex = $auth->createPermission('dashboard/index');
        $viewDashboardProfile = $auth->createPermission('dashboard/profile');

        $viewDashboardIndex->description = 'Просмотр админки';

        // Запишем эти разрешения в БД
        $auth->add($viewDashboardIndex);
        $auth->add($viewDashboardProfile);
        $auth->addChild($admin,  $viewDashboardIndex);
        $auth->addChild($admin,  $viewDashboardProfile);
        $auth->assign($admin, 1);
    }
}