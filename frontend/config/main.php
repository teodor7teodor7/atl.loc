<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //    'enableStrictParsing' => true,
            'rules' => [
                'site/signup' => '/user/registration/register',
                'site/signin' => '/user/security/login',
                'site/login' => '/user/security/login',
                'novosti' => 'news', //From News
                'novosti/<url>' => 'news/one',
                '/' => 'site/index',
                '<controller>/<action>' => '<controller>/<action>',
            ]
        ],
        'urlFrontend' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'http://atl.loc',
        ],
],
    'modules' => [
        'user' => [
            // following line will restrict access to admin controller from frontend application
          'as frontend' => 'dektrium\user\filters\FrontendFilter',
            'controllerMap' => [
                'registration' => [
                    'class' => \dektrium\user\controllers\RegistrationController::className(),
                    'on ' . \dektrium\user\controllers\RegistrationController::EVENT_AFTER_REGISTER => function ($event) {
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole('user');
                        $user = \dektrium\user\models\User::findOne(['username' => $event->form->username]);
                        $auth->assign($role, $user->id);
                    }
                ],

            ],
        ],
    ],
    'params' => $params,
];
