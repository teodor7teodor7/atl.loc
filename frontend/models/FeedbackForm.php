<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Feedback;
/**
 * ContactForm is the model behind the contact form.
 */
class FeedbackForm extends Model
{
    public $name;
    public $email;
    public $text;
    public $city;
    public $topic_id;
    public $exursion;
    public $dateRaw;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_id', 'name', 'email', 'city', 'exursion', 'dateRaw', 'text'], 'required'],
            [['topic_id',], 'integer'],
            [['text'], 'string'],
            [['dateRaw'], 'date', 'format' => 'php:d.m.Y'],
            [['name', 'email', 'city', 'exursion'], 'string', 'max' => 255],
        ];
    }

   public function save()
   {
       $model = new Feedback();
       $model->attributes = $this->attributes;
       $model->status = Feedback::STATUS_OFF;

       if ($model->save()){ return true; }

       return false;
   }

}
