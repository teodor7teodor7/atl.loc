<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\HotelReservation;
use common\behaviors\DateToTimeBehavior;
use yii\db\ActiveRecord;

/**
 * ContactForm is the model behind the contact form.
 */
class HotelReservationForm extends Model
{
    public $number_adults;
    public $number_children;
    public $dateArrivalRaw;
    public $dateDepartureRaw;
    public $type_food;
    public $type_customer;
    public $name_agent;
    public $name_agency;
    public $childrens_age;
    public $email;
    public $date_arrival;
    public $date_departure;
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_customer', 'dateArrivalRaw', 'dateDepartureRaw', 'type_food', 'email', 'phone'], 'required'],
            [['number_adults', 'number_children', 'type_food', 'date_arrival', 'date_departure'], 'integer'],
            ['email', 'email'],
            [['type_customer', 'name_agent', 'name_agency', 'childrens_age', 'email', 'phone'], 'safe'],
            [[ 'dateArrivalRaw', 'dateDepartureRaw'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_customer' => Yii::t('app', 'Type Customer'),
            'name_agent' => Yii::t('app', 'Name Agent'),
            'name_agency' => Yii::t('app', 'Name Agency'),
            'number_adults' => Yii::t('app', 'Number Adults'),
            'number_children' => Yii::t('app', 'Number Children'),
            'date_arrival' => Yii::t('app', 'Date Arrival'),
            'date_departure' => Yii::t('app', 'Date Departure'),
            'type_food' => Yii::t('app', 'Type Food'),
            'childrens_age' => Yii::t('app', 'Childrens Age'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }



    public function save()
    {
        $model = new HotelReservation();
        $model->attributes = $this->attributes;$model->save();
        if ($model->save()){ return true; }

        return false;
    }
}
