<?php

namespace frontend\controllers;

use Yii;
use common\models\Hotel;
use frontend\models\HotelList;
use yii\web\NotFoundHttpException;

class HotelController extends \yii\web\Controller
{
    public function actionHotelList()
    {
        $searchModel = new HotelList();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(!$dataProvider) { throw new NotFoundHttpException();}

        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $searchModel,
        ]);
    }

    public function actionOne($url = null)
    {
        $model = (new Hotel())::find()->andWhere(['url'=>$url])->one();
        if(!$model) { throw new NotFoundHttpException();}
        return $this->render('oneHotel', [
            'model' => $model,
        ]);
    }

}
