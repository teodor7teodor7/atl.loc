<?php

namespace frontend\controllers;
use common\models\Tour;
use yii\web\NotFoundHttpException;

class TourController extends \yii\web\Controller
{

    public function actionList($url = null)
    {
        $model = (new Tour())->getTourList($url);
        if(!$model) { throw new NotFoundHttpException();}
        return $this->render('tourList', [
            'model' => $model,
        ]);
    }

    public function actionOne($url = null)
    {
        $model = (new Tour())->find()->where(['url' => $url])->one();
        if(!$model) { throw new NotFoundHttpException();}
        return $this->render('tourOne', [
            'model' => $model,
        ]);
    }
}
