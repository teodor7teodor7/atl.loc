<?php

namespace frontend\controllers;

use Yii;
use common\models\TourCategory;

class TourCategoryController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = TourCategory::getCategoryUrlList();

        return $this->render('index',
                                 ['model' => $model]);
    }

}
