<?php

namespace frontend\controllers;

use Yii;
use common\models\Page;
use yii\web\NotFoundHttpException;

class PageController extends \yii\web\Controller
{
    public function actionOne($url = null)
    {
        $model = (new Page())::find()->andWhere(['url'=>$url])->one();
        if(!$model) { throw new NotFoundHttpException();}
        return $this->render('onePage', [
            'model' => $model,
        ]);
    }

}
