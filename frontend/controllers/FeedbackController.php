<?php

namespace frontend\controllers;

use common\models\TopicFeedback;
use frontend\models\FeedbackList;
use frontend\models\FeedbackForm;
use Yii;
use common\models\Feedback;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeedbackList();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new FeedbackForm();
        $topic = (new TopicFeedback())->topicList;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->save()) {
                Yii::$app->session->setFlash('success', "Feedback is saved");
                return $this->redirect('feedback');
            }
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'topic' => $topic
        ]);
    }

}
