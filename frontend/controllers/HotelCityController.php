<?php

namespace frontend\controllers;

use frontend\models\HotelList;
use Yii;
use common\models\HotelCity;
use frontend\models\HotelCityList;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HotelCityController implements the CRUD actions for HotelCity model.
 */
class HotelCityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all HotelCity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HotelCityList();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new HotelList();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }
}
