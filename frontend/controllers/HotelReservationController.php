<?php

namespace frontend\controllers;

use frontend\models\HotelReservationForm;
use Yii;
use common\models\HotelReservation;
use backend\models\HotelReservationSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * HotelReservationController implements the CRUD actions for HotelReservation model.
 */
class HotelReservationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all HotelReservation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new HotelReservationForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->save()){
                Yii::$app->session->setFlash('success', 'Add order');
                return $this->redirect('/hotel-city');
            }


        }

        $method = Yii::$app->request->isAjax ? 'renderAjax' : 'render';
        return $this->renderAjax('index', [
            'model' => $model,
        ]);
    }

    public function actionValidationHotelReservationForm() {
        $model = new HotelReservationForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }


}
