<?php

namespace frontend\controllers;

use Yii;
use common\models\News;
use frontend\models\NewsList;
use yii\web\NotFoundHttpException;

class NewsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new NewsList();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionOne($url = null)
    {
        $model = (new News())::find()->andWhere(['url'=>$url])->one();
        if(!$model) { throw new NotFoundHttpException();}
        return $this->render('oneNews', [
            'model' => $model,
        ]);
    }

}
