<?php
/* @var $this yii\web\View */

use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php
    $form = ActiveForm::begin(['id' => $model->formName(),
        'action' => 'hotel/hotel-list',
        'method' => 'get'

    ]);
?>
<?= $form->field($model, 'reting_id')->dropDownList(common\models\Hotel::getHotelRetingList(), ['prompt' =>'Please select...']); ?>
<?= $form->field($model, 'city_id')->dropDownList(common\models\Hotel::getHotelCityList(), ['prompt' =>'Please select...']); ?>

<?= Html::submitButton('Search', ['class' => 'btn btn-info', 'name' => 'signup-button']) ?>
<?php ActiveForm::end(); ?>


<h2>Hotel City</h2>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_city',
]); ?>
