<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>

<div class="city">
    <b><?= Html::a(Html::encode($model->name), Url::toRoute(['hotel/hotel-list', 'city' => $model->id], true)); ?></b>

</div>
