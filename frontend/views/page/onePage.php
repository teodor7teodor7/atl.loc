<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>

<div class="one-news">

    <b><?= $model->title ?></b>
    <?= $model->text ?>
</div>
