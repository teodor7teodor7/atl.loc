<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>

<div class="one-news">
<?php foreach ($model as $one){
echo Html::a(Html::encode($one->title), Url::toRoute(['tour/one', 'url' => $one->url], true));
echo '<br/>';
}
?>
</div>
