<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>

<div class="city">
    <b><?= Html::a(Html::encode($model->name), Url::toRoute(['hotel/one', 'url' => $model->url], true)); ?></b>

     <?= $model->text ?>
</div>