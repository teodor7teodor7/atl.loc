<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use \metalguardian\fotorama\Fotorama;
?>

<div class="one-hotel">

    <b><?= $model->name ?></b>
    <?= $model->text ?>

    <?php
    $fotorama = Fotorama::begin(
        [
            'options' => [
                'loop' => true,
                'hash' => true,
                'ratio' => 50/10,
            ],
            'spinner' => [
                'lines' => 20,
            ],
            'tagName' => 'span',
            'useHtmlData' => false,
            'htmlOptions' => [
                'class' => 'custom-class',
                'id' => 'custom-id',
            ],
        ]
    );
    foreach ($model->hotelImgList as $one){
        echo  Html::img(Yii::$app->urlFrontend->baseUrl . '/images/hotel/'. $one->name, ['alt' => $one->name]);
    }
    ?>

    <?php $fotorama->end(); ?>
</div>
<?= Html::button('Reservetion', ['value' => Url::to(['/hotel-reservation']), 'title' => 'Reservetion hotel', 'class' => 'showModalButton btn btn-success']); ?>
<?php
yii\bootstrap\Modal::begin([
    'header' => '<div class="header-content"><h4 class="modal-title" id="modalHeader"></h4></div>',
    'headerOptions' => ['class' => 'modal-header',],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
]);
echo '<div id="modalContent"><div style="text-align:center"><i class="fa fa-refresh fa-spin"></i></div></div>';
yii\bootstrap\Modal::end();
?>
<?php $this->registerJsFile('/js/modalWindow.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
