<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use common\models\HotelReservation;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\HotelReservation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hotel-reservation-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName(),
        'enableAjaxValidation' => true,
        'validationUrl' => Url::toRoute('validation-hotel-reservation-form'),
         ]); ?>

    <div class="row">
    <div class="col-xs-6">
    <?= $form->field($model, 'type_customer')->dropDownList(HotelReservation::getTypeCustomer()); ?>

    <?= $form->field($model, 'name_agent')->textInput() ?>

    <?= $form->field($model, 'name_agency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_adults')->dropDownList([
        '1', '2', '3', '4', '5'
    ]); ?>


    <?= $form->field($model, 'number_children')->dropDownList([
        '0', '1', '2', '3', '4', '5'
    ]); ?>


    <?= $form->field($model, 'dateArrivalRaw')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter arrival date ...'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]); ?>
</div>    <div class="col-xs-6">
    <?= $form->field($model, 'dateDepartureRaw')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter departure date ...'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]);?>

    <?= $form->field($model, 'type_food')->dropDownList(HotelReservation::getTypeFood()); ?>

    <?= $form->field($model, 'childrens_age')->dropDownList([
        '0', '1', '2', '3', '4', '5'
    ]); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    </div>  </div>
    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJsFile('/js/reservationHotel.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
