<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>

<div class="news">

    <b><?= Html::a(Html::encode($model->title), Url::to(['news/one', 'url' => $model->url],true)) ?></b>
</div>
