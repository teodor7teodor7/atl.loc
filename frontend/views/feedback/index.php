<?php
/* @var $this yii\web\View */
use yii\widgets\ListView;
?>
<h2>Feedback</h2>
    <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_feedback',
    ]); ?>
    <?= $this->render('_form', [
        'model' => $model,
        'topic' => $topic
    ]) ?>

