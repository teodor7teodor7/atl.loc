<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\select2\Select2;
    use kartik\date\DatePicker;

    /* @var $this yii\web\View */
    /* @var $model common\models\Feedback */
    /* @var $form yii\widgets\ActiveForm */

    $params = [
        '1' => 'On',
        '0' => 'Off',
    ];
    ?>

    <div class="feedback-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'topic_id')->widget(Select2::classname(), [
            'data' => $topic,
            'options' => ['placeholder' => 'Select a topic ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'exursion')->textInput(['maxlength' => true]) ?>

        <?= DatePicker::widget([
            'name' => $model->formName() . '[dateRaw]',
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd.mm.yyyy'
            ]
        ]); ?>

   <br/>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
