<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>

<div class="feedback">
    <b><?= $model->topicFeedback->title?></b><br/>
    <u><?= $model->text?></u><p/>
    <u><?= $model->name?></u><p/>
</div>
