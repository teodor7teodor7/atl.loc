<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>
<h2>Category Tours</h2>
<div class="one-news">

    <?php foreach ($model as $url=>$title){
        echo Html::a(Html::encode($title), Url::toRoute(['tour/list', 'url' => $url], true));
        echo '<br/>';
    }
    ?>
</div>
