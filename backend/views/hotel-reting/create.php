<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HotelReting */

$this->title = Yii::t('app', 'Create Hotel Reting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hotel Retings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-reting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
