<?php
use kartik\file\FileInput;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

$form = ActiveForm::begin([
        'id' => $model->formName()
    ]);
?>

<?= $form->field($model, 'file')->fileInput()->label('Avatar'); ?>

<?= Html::submitButton( 'Close', ['class' =>  'btn btn-default' ,'data-dismiss'=>"modal" ]) ?>
<?= Html::submitButton( 'Save changes', ['class' =>  'pull-right btn btn-primary' ])  ?>

<?php ActiveForm::end(); ?>
