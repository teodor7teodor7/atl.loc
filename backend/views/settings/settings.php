<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = Yii::t('user', 'User settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->registerJsFile('/js/adminlte/settingsPage.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-body box-profile">
                <?php if(!empty(Yii::$app->user->identity->profile->avatar)){ ?>
                    <img class="profile-user-img img-responsive img-circle"
                         src="/uploads/images/user_<?= Html::encode($modelProfile->user_id) ?>/avatar/<?= Html::encode($modelProfile->avatar) ?>" alt="User profile picture">
                <?php } else {?>
                    <img src="/images/user_avatar.png" class="profile-user-img img-responsive img-circle" alt="User Image"/>
                <?php }?>
                <?php if (!empty($modelProfile->name)): ?>
                <h3 class="profile-username text-center"><?= Html::encode($modelProfile->name) ?></h3>
                <?php endif; ?>
                <p class="text-muted text-center"><?= Html::encode($modelProfile->headline) ?></p>
                <?= Html::button('Edit photo',  ['value' => Url::to(['settings/upload-avatar']), 'title' => 'Edite user photo', 'class' => 'showModalButton btn btn-primary btn-block']) ?>
            </div>
        </div>
        <?//= $this->render('_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
                    <li><a data-toggle="tab" href="#account">Accaunt</a></li>
                </ul>

            <div class="tab-content">
                    <div class="active tab-pane" id="profile">
                <?= $this->render('_profile', [
                        'model' => $modelProfile,
                        'module' => Yii::$app->getModule('user')
                ]) ?>
                    </div>
                <div class="tab-pane" id="account">
                <?= $this->render('_account', [
                    'model' => $modelAccount,
                    'module' => Yii::$app->getModule('user')
                ]) ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
