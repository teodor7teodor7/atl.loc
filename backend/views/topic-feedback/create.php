<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TopicFeedback */

$this->title = 'Create Topic Feedback';
$this->params['breadcrumbs'][] = ['label' => 'Topic Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topic-feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
