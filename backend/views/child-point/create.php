<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ChildPoint */

$this->title = Yii::t('app', 'Create Child Point');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Child Points'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-point-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
