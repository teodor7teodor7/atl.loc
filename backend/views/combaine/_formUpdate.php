<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\Combaine;
use kartik\date\DatePicker;
use yii\bootstrap\Collapse;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Combaine */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('../js/bootstrap-datepicker.js');
$this->registerCssFile('../css/bootstrap-datepicker.css');
$this->registerJs('
    $(".datepicker-date").datepicker({
    multidate: true
    });');
$script = <<< JS
 var daysWeek = [1,2,3,4,5,6,7];
 var daysNameWeek = ['','sun','mon','wen','thu','fri','wen','sat'];
 $("#combaine-region_id").change(function(){render()});
      render();
       var daysTimeField = $(".days-time-field").clone();
       $(".days-time-field").remove();
       var timeBlock = $("#time-collapse-default").clone();
       $("#time-collapse-default").remove();
       $(timeBlock).attr("id", "time-collapse");
       $(timeBlock).find('.panel-collapse').attr("id", "time-collapse");       
       var timeField = $(".time-field").clone();
       $(".time-field").remove();     
 function render (){      
        var region = $("#combaine-region_id").val();
        if (isFinite(region) == true) {
            $("#points").empty();
            $.ajax({
                url: "/combaine/cities-region-list-ajax?id=" + region,
                dataType : "json",
                success: function (data, textStatus) {
                     cities(data);
                }               
            });
       $.ajax({
            url: "/combaine/point-time?id=" + region,
            dataType : "json",
            success: function (data, textStatus) {   
               points(data);
            }               
        });
       $.ajax({
            url: "/combaine/point-time-list-ajax?id=" + location.search.replace('?id=',''),
            dataType : "json",
            success: function (data, textStatus) {  
             times(data);
            }               
        }); 
    }
    function points(data){
      var points = '';
      for (var key in data) {
        var result =  data[key];
            for (var keyField in result) {                 
                var time = timeField.clone();
                    time.find('#time-input').attr("id", "time-input-" + keyField); 
                    var timeNameField = 'Combaine[time][' + keyField + ']';
                    time.find('#time-input-' + keyField).attr('name', timeNameField);
                    time.find('#time-input-' + keyField).addClass('time-input-field');
                    time.find('#time_switch').attr("id", "time_switch-" + keyField);
                    time.find('.control-label').text(result[keyField]);                   
                    time.find('.field-time-input').attr("data-switch", "time_switch-" + keyField);                  
                    daysWeek.forEach(function(item, i, arr) {
                        var timeDays = daysTimeField.clone();
                        timeDays.find('#days-time-input').attr("id", 'days-time-input-' + keyField +'-'+item); 
                        var timeDaysNameField = "Combaine[daysTime][" + keyField + "]["+ item+ "]";
                        timeDays.find("#days-time-input-" + keyField + "-" + item).attr("name", timeDaysNameField);
                        timeDays.find('.control-label').text(daysNameWeek[item]);
                        timeDays.find( ".field-days-time-input" ).addClass( "time_switch-" + keyField );
                        timeDays.find( ".field-days-time-input" ).find( "input" ).addClass( "time-input-" + keyField);
                        timeDays.find(".field-days-time-input").hide();
                        $('#time-collapse'+ key).find('.panel-body').append(timeDays.html());                
                        $("#days-time-input-" + keyField + "-" + item).timepicker();         
                    })
                    $('#time-collapse'+ key).find('.panel-body').append(time.html());     
                    $("#time-input-" + keyField).timepicker();
                    $("#time_switch-" + keyField).bootstrapSwitch({
                        on: 'On',
                        off: 'Off',
                        onSwitchChange: function(data) {
                             if(data.target.checked == true){
                                $('[data-switch="' + this.id +'"]').hide();
                                
                                console.log( this.id);
                                
                                $( "." + this.id ).show() 
                             } else{  $( "." + this.id ).hide() ;

                             $('[data-switch="'+ this.id +'"]').find('input').timepicker('setTime', '12:00 AM');
                              $('.'+ this.id).find('input').timepicker('setTime', '12:00 AM');
                                $('[data-switch="' + this.id+'"]').show();
                             } 
                        }
     });         
    $('#time-collapse-'+key).removeClass('hidden');
            }                       
          }
        }
    function cities(data){
      data.results.forEach(function(item, i, arr) {
          var city = timeBlock.clone();
          city.attr("id", "time-collapse-" + item.id);
          city.find('#time-collapse').attr("id", "time-collapse" + item.id);
          city.find('.collapse-toggle').attr("href", "#time-collapse" + item.id);
          city.find('.collapse-toggle').attr("data-parent", "time-collapse" + item.id);
          city.find('a.collapse-toggle').text(item.text);
          $("#points").append(city);      
      });
    }
     function times(data){
        for (var key in data) {
               var result =  data[key];
             for (var keyTime in result) {
                  $("#days-time-input-" +  key + "-" + keyTime).timepicker('setTime',result[keyTime]);                                 
             }
              if(dif(result)== false){
                         $("#time_switch-" + key).bootstrapSwitch('state', true);}           
        }
     }
    function dif(data){
          for (var key in data) {
                if(data[1] != data[key]){ return false}}
          }
 } 
 $("body").on('changeTime.timepicker', '.time-input-field', function(e) {
    if(this.value) {
        $('.' + this.id).timepicker('setTime', this.value);
    }
  });
JS;

 $this->registerJs($script);

?>
<?php

$switchInputValue = true;
if( $model->city_switch == Combaine::CITY_SWITCH_OFF) {
    $switchInputValue = false;
    $this->registerJs("jQuery(document).ready(function($){ $( '.field-cities' ).hide() });");
}

$params = [
        'prompt' => 'Please select status...'
    ];
$url = \yii\helpers\Url::to(['cities-region-list-ajax']);
?>

<div class="combaine-form">

    <?php $form = ActiveForm::begin([
            'id' => $model->formName(),
    ]); ?>

    <?= $form->field($model, 'excursion_id')->dropDownList($excursion, $params)->label('Excursion'); ?>

    <?= $form->field($model, 'region_id')->dropDownList($region, $params)->label('Region'); ?>

    <?php echo SwitchInput::widget([
             'name' => $model->formName() . '[city_switch]',
             'type' => SwitchInput::CHECKBOX,
                'value' => $switchInputValue,
                'options' => ['id'=> 'city_switch'],
                'pluginEvents' => [
                    "switchChange.bootstrapSwitch" => "function() {  ($('#city_switch').bootstrapSwitch('state') == false) 
                            ? $( '.field-cities' ).hide() : $( '.field-cities' ).show(); }"
                ],

        ]); ?>

    <?php
    $model->city =  $model->excursionCityList;
    $data = $model->cityList;
    echo $form->field($model, 'city')->widget(Select2::classname(), [
        'data' => $data,
        'maintainOrder' => true,
        'showToggleAll' => true,
        'options' => ['placeholder' => 'Select...', 'id' => 'cities', 'multiple' => true, ],
        'pluginOptions' => [
            'type' => 'true',
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) {
                                             var region = $("#combaine-region_id").val();
                                             return { id:region }
                                            }')
            ],
        ],
    ]);
    ?>
    <h3>Date of excursion</h3>
    <?php

    if(isset($model->date_start)) {
        $model->date_start = date('d.m.Y', $model->date_start);
    }
    else $model->date_start = date('d.m.Y');

    echo $form->field($model, 'date_start')->widget(DatePicker::classname(),[
        'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd.mm.yyyy',
        ]
    ]); ?>

    <?php  $model->days = $model->excursionDaysSelect;

        echo $form->field($model, 'days')->widget(Select2::classname(), [
        'data' => $model->daysWeek,
        'options' => ['placeholder' => 'Select...', 'id' => 'date', 'multiple' => true, ],
    ]);
    ?>

    <?php echo $form->field($model, 'period_data')->dropDownList($model->periodData, $params)->label('Period'); ?>
    <?php echo $form->field($model, 'period_type')->dropDownList($model->periodType, $params)->label('Type Period'); ?>

    <?php $model->date =  $model->excursionDateSelect;
    echo $form->field($model, 'date')->textInput(['maxlength' => true, 'class' => 'datepicker-date form-control']); ?>

    <?php $model->exception =  $model->excursionExceptionSelect;
    echo $form->field($model, 'exception')->textInput(['maxlength' => true, 'class' => 'datepicker-date form-control']); ?>

    <h3>Type of Price</h3>
    <?php
    $prices =  $model->excursionPrices;

        foreach(\common\models\TypeCost::getTypeCostList() as $key => $one) {
            ?>
        <?= $form->field($model, 'price[' . $key .']')->textInput(['maxlength' => true, 'value' => isset($prices[$key]) ? $prices[$key] : '0.00' ])->label($one) ?>

        <?php } ?>

    <h3>Time of Point</h3>

    <?php $timeField =''; $timeField .=  $form->field($model, 'time[]')->widget(TimePicker::classname(),
        ['options' => [
            'id' => 'time-input',

                'showSeconds' => false,
            'template' => '<div class="form-group">{input}<label class="control-label">{label}</label></div>'
            ],

        ]);
    ?>


    <?php $timeField .= SwitchInput::widget([
        'name' => $model->formName() . '[time_switch]',
        'type' => SwitchInput::CHECKBOX,
        'value' => $switchInputValue,
        'options' => ['id'=> 'time_switch'],
        'pluginEvents' => [
                "switchChange.bootstrapSwitch" => "function() {
                 console.log(1);}"
        ],

    ]); ?>
    <div class="time-field">
        <?php echo $timeField  ?>
    </div>
    <div class="days-time-field">
        <?php echo $form->field($model, 'daysTime[]')->widget(TimePicker::classname(),
            ['options' => [
                'id' => 'days-time-input',

                'showSeconds' => false,
                'template' => '<div class="form-group">{input}<label class="control-label">{label}</label></div>'
            ],

            ]);
        ?>

        </div>
   <?php echo Collapse::widget([ 'items' => [
               [
                   'label' => 'Time',
                   'content' => '',
                   'contentOptions' => ['class' => 'default'],

               ],
   ],
       'options' => ['id' => 'time-collapse-default', 'class' => 'time-collapse hidden'],
       ]);
   ?>

    <div id="points">
        <?php echo 'Please select region ...' ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
