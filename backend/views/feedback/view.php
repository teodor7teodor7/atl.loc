<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Feedback */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            ['attribute' => 'topicFeedback.title',
                'label' => 'Topic'
            ],
            'name',
            'email:email',
            'text:ntext',
            'city',
            'exursion',
            ['attribute' => 'date',
                'label'  => 'Date',
                'format' => ['date', 'php:d.m.Y']],
            'answer:ntext',
            'status',
            ['attribute' => 'created_at',
                'label'  => 'Created',
                'format' => ['date', 'php:d.m.Y']],
            ['attribute' => 'updated_at',
                'label'  => 'Updated',
                'format' => ['date', 'php:d.m.Y']],

        ],
    ]) ?>

</div>
