<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AuthAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Type');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'user_id',
            [
                'attribute' => 'userInfo.name',
                'value' => 'userInfo.username'
            ],
            [
                'attribute' => 'email',
                'value' => 'userInfo.email'
            ],
            [ 'attribute' => 'item_name',
                'value' => function ($data) {
                    return User::getUserType()[$data->item_name]; },
            'filter'=> User::getUserType(),
                'label' => 'User role'],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
