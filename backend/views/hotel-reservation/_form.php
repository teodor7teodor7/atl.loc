<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HotelReservation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hotel-reservation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type_customer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_agent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_agency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_adults')->textInput() ?>

    <?= $form->field($model, 'number_children')->textInput() ?>

    <?= $form->field($model, 'date_arrival')->textInput() ?>

    <?= $form->field($model, 'date_departure')->textInput() ?>

    <?= $form->field($model, 'type_food')->textInput() ?>

    <?= $form->field($model, 'childrens_age')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
