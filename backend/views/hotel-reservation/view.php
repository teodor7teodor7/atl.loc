<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\HotelReservation;
/* @var $this yii\web\View */
/* @var $model common\models\HotelReservation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hotel Reservations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-reservation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type_customer',
            [
                'attribute' => 'ype_customert',
                'value' => HotelReservation::getTypeCustomer()[$model->type_customer]
            ],
            'name_agent',
            'name_agency',
            'number_adults',
            'number_children',
            ['attribute' =>   'date_arrival',
                'label'  => 'Date arrival',
                'format' => ['date', 'php:d.m.Y']],
            ['attribute' => 'date_departure',
                'label'  => 'Date departure',
                'format' => ['date', 'php:d.m.Y']],
            [
                'attribute' => 'type_food',
                'value' => HotelReservation::getTypeFood()[$model->type_food]
            ],
            'childrens_age',
            'email:email',
            'phone',
            ['attribute' => 'created_at',
                'label'  => 'Created',
                'format' => ['date', 'php:d.m.Y']],
            ['attribute' => 'updated_at',
                'label'  => 'Updated',
                'format' => ['date', 'php:d.m.Y']],
        ],
    ]) ?>

</div>
