<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HotelReservation */

$this->title = Yii::t('app', 'Create Hotel Reservation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hotel Reservations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-reservation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
