<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\HotelReservation;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HotelReservationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Hotel Reservations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-reservation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //= Html::a(Yii::t('app', 'Create Hotel Reservation'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'type_customer',  'filter' => HotelReservation::getTypeCustomer(),
                'value' => function ($data) {
                    return HotelReservation::getTypeCustomer()[$data->type_customer]; },
            ],
            'name_agent',
            'name_agency',
            'number_adults',
            //'number_children',
            //'date_arrival',
            //'date_departure',
            //'type_food',
            //'childrens_age',
            //'email:email',
            //'phone',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
