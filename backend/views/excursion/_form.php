<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Excursion */
/* @var $form yii\widgets\ActiveForm */


$this->registerJsFile('../js/jquery.liTranslit.js');
$this->registerJsFile('../js/translit.js');
?>

<div class="excursion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'class' => 'translit form-control']) ?>
    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'imageFiles[]')->widget(FileInput::classname(),[
        'options' => ['multiple' => true],
        'pluginOptions' => [
            'deleteUrl' => Url::to(['/image-manager/delete-image']),
            'previewFileType' => 'any',
           'initialPreview'=> $model->imgLinkList,
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'uploadClass' => 'hide',
            'overwriteInitial'=>false,
            'initialPreviewAsData'=>true,
            'maxFileCount' => 10,
            'initialPreviewConfig' => $model->imgConfigList,
        ],
    ]); ?>
    <?php
    $model->direction = $model->directionSelectedList;
    echo $form->field($model, 'direction')->widget(Select2::classname(), [
        'data' => $model->directionList,

        'options' => ['multiple' => true, 'placeholder' => 'Select ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?php
    $model->typeExcursion = $model->typeExcursionSelectedList;
    echo $form->field($model, 'typeExcursion')->widget(Select2::classname(), [
        'data' => $model->typeList,
        'options' => ['multiple' => true, 'placeholder' => 'Select ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?php
    $model->attraction = $model->attractionSelectedList;
    echo $form->field($model, 'attraction')->widget(Select2::classname(), [
        'data' => $model->attractionList,
        'options' => ['multiple' => true, 'placeholder' => 'Select ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'class' => 'url form-control']) ?>

    <br/>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
