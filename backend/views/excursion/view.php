<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use metalguardian\fotorama\Fotorama;

/* @var $this yii\web\View */
/* @var $model app\models\Excursion */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Excursions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="excursion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'text'
        ],
    ]) ?>
    <?php
    $fotorama = Fotorama::begin(
        [
            'options' => [
                'loop' => true,
                'hash' => true,
                'ratio' => 400/300,
            ],
            'spinner' => [
                'lines' => 20,
            ],
            'tagName' => 'span',
            'useHtmlData' => false,
            'htmlOptions' => [
                'class' => 'custom-class',
                'id' => 'custom-id',
            ],
        ]
    );
    foreach ($model->imgList as $one){
        echo  Html::img(Yii::$app->urlManagerFrontend->baseUrl . '/images/excursion/'. $one->name, ['alt' => $one->name]);
    }
    ?>

    <?php $fotorama->end(); ?>
</div>
