<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Hotel */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('../js/jquery.liTranslit.js');
$this->registerJsFile('../js/translit.js');
?>

<div class="hotel-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'translit form-control']) ?>

    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'class' => 'url form-control']) ?>

    <?= $form->field($model, 'reting_id')->widget(Select2::classname(), [
        'data' => $model->hotelRetingList,
        'options' => ['placeholder' => 'Select a reting ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'city_id')->widget(Select2::classname(), [
        'data' => $model->hotelCityList,
        'options' => ['placeholder' => 'Select a city ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('City'); ?>
    <?= $form->field($model, 'reting_type')->dropDownList($model->types) ?>

    <?= $form->field($model, 'status')->dropDownList($model->statuses)?>

    <?= $form->field($model, 'imageFiles[]')->widget(FileInput::classname(),[
        'options' => ['multiple' => true],
        'pluginOptions' => [
            'deleteUrl' => Url::to(['/image-manager/delete-image']),
            'initialPreview'=> $model->hotelImgLinkList,
            'previewFileType' => 'any',
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'uploadClass' => 'hide',
            'overwriteInitial'=>false,
            'initialPreviewAsData'=>true,
            'maxFileCount' => 10,
            'initialPreviewConfig' => $model->hotelImgConfigList,

        ],
    ]); ?>
    <br/>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
