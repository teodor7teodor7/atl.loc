<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use metalguardian\fotorama\Fotorama;
/* @var $this yii\web\View */
/* @var $model common\models\Hotel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hotels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'text:html',
                'url',
                'reting_id',
                'reting_type',
                'status',
                ['attribute' => 'created_at',
                    'label'  => 'Created',
                    'format' => ['date', 'php:d.m.Y']],
                ['attribute' => 'updated_at',
                    'label'  => 'Updated',
                    'format' => ['date', 'php:d.m.Y']],
            ],
        ]) ?>
<?php
    $fotorama = Fotorama::begin(
            [
                'options' => [
                    'loop' => true,
                    'hash' => true,
                    'ratio' => 400/300,
                ],
                'spinner' => [
                    'lines' => 20,
                ],
                'tagName' => 'span',
                'useHtmlData' => false,
                'htmlOptions' => [
                    'class' => 'custom-class',
                    'id' => 'custom-id',
                ],
            ]
        );
    foreach ($model->hotelImgList as $one){
       echo  Html::img(Yii::$app->urlManagerFrontend->baseUrl . '/images/hotel/'. $one->name, ['alt' => $one->name]);
    }
    ?>

    <?php $fotorama->end(); ?>

</div>
