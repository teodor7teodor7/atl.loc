<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;
use kartik\select2\Select2;
use common\models\City;
/* @var $this yii\web\View */
/* @var $model common\models\PickupPoint */
/* @var $form yii\widgets\ActiveForm */

$switchInputValue = true;
if( $model->child_switch == \common\models\PickupPoint::CITY_SWITCH_OFF) {
    $switchInputValue = false;
    $this->registerJs("jQuery(document).ready(function($){ $( '.field-childPoint' ).hide() });");
}

$params = [
    'prompt' => 'Please select status...'
];
?>
<div class="pickup-point-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo SwitchInput::widget([
        'name' => $model->formName() . '[child_switch]',
        'type' => SwitchInput::CHECKBOX,
        'value' => $switchInputValue,
        'options' => ['id'=> 'child_switch'],
        'pluginEvents' => [
            "switchChange.bootstrapSwitch" => "function() {  ($('#child_switch').bootstrapSwitch('state') == false) 
                            ? $( '.field-childPoint' ).hide() : $( '.field-childPoint' ).show(); }"
        ],

    ]); ?>

   <?php
    $model->childPoint = $model->childPointSelect;
    echo $form->field($model, 'childPoint')->widget(Select2::classname(), [
        'data' => $model->getChildPointList(),
        'options' => ['id' => 'childPoint', 'multiple' => true, 'placeholder' => 'Select ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?php
    $cityList = City::getCityList();
    echo $form->field($model, 'city_id')->dropDownList($cityList, $params); ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
