<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PickupPoint */

$this->title = Yii::t('app', 'Create Pickup Point');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pickup Points'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pickup-point-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
