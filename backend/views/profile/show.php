<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = 'User Profile';
$this->params['breadcrumbs'][] = $this->title;
$modelProfile = Yii::$app->user->identity->profile;
?>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-body box-profile">
                <?php if(!empty(Yii::$app->user->identity->profile->avatar)){ ?>
                    <?= Html::img("/uploads/images/user_".  Html::encode($modelProfile->user_id) ."/avatar/". Html::encode($modelProfile->avatar), [
                        'class' => 'profile-user-img img-responsive img-circle',
                        'alt' => $profile->name,
                    ]) ?>
                <?php } else {?>
                    <img src="/images/user_avatar.png" class="profile-user-img img-responsive img-circle" alt="User Image"/>
                <?php }?>

                <?php if (!empty($profile->name)): ?>
                    <h3 class="profile-username text-center"><?= Html::encode($profile->name) ?></h3>
                <?php endif; ?>
                <p class="text-muted text-center"><?= Html::encode($profile->headline) ?></p>



            </div>
        </div>
        <? //= $this->render('_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About me</h3>
            </div>
            <div class="box-body">

                <?php if (!empty($profile->location)): ?>
                    <strong><i class="fa fa-map-marker margin-r-5"></i>Location</strong>
                    <p class="text-muted">
                        <?= Html::encode($profile->location) ?>
                    </p>
                    <hr>
                <?php endif; ?>
                <?php if (!empty($profile->website)): ?>
                    <strong><i class="fa   fa-desktop  margin-r-5"></i>Web site</strong>
                    <p class="text-muted">
                        <?= Html::a(Html::encode($profile->website), Html::encode($profile->website)) ?>
                    </p>
                    <hr>
                <?php endif; ?>
                <?php if (!empty($profile->public_email)): ?>
                    <strong><i class="fa  fa-envelope margin-r-5"></i>E-mail</strong>
                    <p class="text-muted">
                        <?= Html::a(Html::encode($profile->public_email),
                            'mailto:' . Html::encode($profile->public_email)) ?>
                    </p>
                    <hr>
                <?php endif; ?>

                <!--                        <i class="glyphicon glyphicon-time text-muted"></i> --><? //= Yii::t('user', 'Joined on {0, date}', $profile->user->created_at) ?>



                <?php if (!empty($profile->bio)): ?>
                    <strong><i class="fa fa-file-text-o margin-r-5"></i>Bio</strong>
                    <p class="text-muted"> <?= Html::encode($profile->bio) ?></p>
                    <hr>
                <?php endif; ?></div>
        </div>
    </div>
</div>

