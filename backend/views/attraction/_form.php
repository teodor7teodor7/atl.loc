<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Attraction */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('../js/jquery.liTranslit.js');
$this->registerJsFile('../js/translit.js');
?>

<div class="attraction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'class' => 'translit form-control']) ?>

    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'imageFiles')->widget(FileInput::classname(),[
        'options' => ['multiple' => true],
        'pluginOptions' => [
            'deleteUrl' => Url::to(['/image-manager/delete-image']),
            'previewFileType' => 'any',
            'initialPreview'=> $model->imageLink,
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'uploadClass' => 'hide',
            'overwriteInitial'=>false,
            'initialPreviewAsData'=>true,
            'maxFileCount' => 1,
            'initialPreviewConfig' => $model->imageConfig,
        ],
    ]); ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'class' => 'url form-control']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
