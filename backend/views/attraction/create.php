<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Attraction */

$this->title = Yii::t('app', 'Create Attraction');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attractions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attraction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
