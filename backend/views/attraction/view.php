<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Attraction */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attractions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attraction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'text:ntext',
            'url:url',
            'created_at',
            'updated_at',
        ],
    ]) ?>
    <?php
     foreach ($model->image as $one){
        echo  Html::img(Yii::$app->urlManagerFrontend->baseUrl . '/images/attraction/'. $one->name, ['alt' => $one->name]);
    }
    ?>
</div>
