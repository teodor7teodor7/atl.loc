<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TypeExcursion */

$this->title = Yii::t('app', 'Create Type Excursion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Type Excursions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-excursion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
