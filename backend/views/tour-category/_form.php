<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TourCategory */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('../js/jquery.liTranslit.js');
$this->registerJsFile('../js/translit.js');
?>

<div class="tour-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true,  'class' => 'translit form-control']) ?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'class' => 'url form-control']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
