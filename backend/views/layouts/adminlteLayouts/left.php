<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'News', 'icon' => 'newspaper-o', 'url' => ['/news']],
                    [
                        'label' => 'Pages',
                        'icon' => 'file-text-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Pages', 'icon' => 'file-text-o', 'url' => ['/page'],],
                            ['label' => 'Category', 'icon' => 'file-text-o', 'url' => ['/page-category'],],
                        ],
                    ],
                    [
                        'label' => 'Feedback',
                        'icon' => 'comments-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Feedback', 'icon' => 'comments-o', 'url' => ['/feedback'],],
                            ['label' => 'Topic', 'icon' => 'file-text-o', 'url' => ['/topic-feedback'],],
                            ],
                    ],
                    [
                        'label' => 'Hotels',
                        'icon' => 'hotel',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Hotel', 'icon' => 'hotel', 'url' => ['/hotel'],],
                            ['label' => 'City', 'icon' => 'map-signs', 'url' => ['/hotel-city'],],
                            ['label' => 'Reting', 'icon' => 'area-chart', 'url' => ['/hotel-reting'],],
                            ['label' => 'Reservation', 'icon' => 'shopping-cart', 'url' => ['/hotel-reservation'],],
                        ],
                    ],
                    [
                        'label' => 'Tours',
                        'icon' => 'file-text-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Tours', 'icon' => 'file-text-o', 'url' => ['/tour'],],
                            ['label' => 'Category', 'icon' => 'file-text-o', 'url' => ['/tour-category'],],
                        ],
                    ],
                    [
                        'label' => 'Excursion',
                        'icon' => 'globe',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Excursion', 'icon' => 'globe', 'url' => ['/excursion']],
                            ['label' => 'City', 'icon' => 'building', 'url' => ['/city']],
                            ['label' => 'Region', 'icon' => 'map-o', 'url' => ['/region']],
                            ['label' => 'Add city in region', 'icon' => 'square-o', 'url' => ['/region-city']],
                            ['label' => 'Type of cost', 'icon' => 'money', 'url' => ['/type-cost']],
                            ['label' => 'Type of excursion', 'icon' => 'gear', 'url' => ['/type-excursion']],
                            ['label' => 'Combaine', 'icon' => 'globe', 'url' => ['/combaine']],
                            ['label' => 'Direction', 'icon' => 'map-signs', 'url' => ['/direction']],
                            ['label' => 'Attraction', 'icon' => 'tree', 'url' => ['/attraction'],],
                            ['label' => 'Pickup point', 'icon' => 'map-o', 'url' => ['/pickup-point'],],
                            ['label' => 'Child point', 'icon' => 'map-o', 'url' => ['/child-point'],],
                        ],
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Users settings',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'User', 'url' => ['/user/admin/index'], 'icon' => 'user'],
                            ['label' => 'Users group', 'url' => ['/auth-assignment'], 'icon' => 'users'],
                            ]
                    ],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
