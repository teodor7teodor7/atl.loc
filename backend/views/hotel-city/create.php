<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HotelCity */

$this->title = Yii::t('app', 'Create Hotel City');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hotel Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
