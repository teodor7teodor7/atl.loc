<?php
namespace backend\controllers;

use Yii;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\DynamicModel;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use common\models\ImageManager;

/**
 * Image manager controller
 */
class ImageManagerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','image-upload','file-upload', 'image-manager-upload','delete-image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Image Upload
     *
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionImageUpload()
    {
        $this->enableCsrfValidation = false;
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $dir = Yii::getAlias('@frontend') . '/web/images/news/';
            if(!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            $resultLink = Yii::$app->urlManagerFrontend->baseUrl.'/images/news/';
            $file = UploadedFile::getInstanceByName('file');
            $model = new DynamicModel(['file' => $file]);
            $model->addRule('file', 'image')->validate();
            if ($model->hasErrors()) {
                $result = [
                    'error' => $model->getFirstError('file'),
                ];
            } else {
                if (file_exists($dir . $model->file->name)) {
                    return [
                        'error' => Yii::t('vova07/imperavi', 'ERROR_FILE_ALREADY_EXIST'),
                    ];
                }
                $model->file->name = uniqid() . '.' . $model->file->extension;
                if ($model->file->saveAs($dir . $model->file->name)) {
                    $result = ['id' => $model->file->name, 'filelink' => $resultLink . $model->file->name];

                } else {
                    $result = [
                        'error' => Yii::t('vova07/imperavi', 'ERROR_CAN_NOT_UPLOAD_FILE'),
                    ];
                }
            }
            return $result;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }
    /**
     * Image Carousel Upload
     *
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionImageManagerUpload()
    {
        $this->enableCsrfValidation = false;
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            Yii::$app->response->format = Response::FORMAT_JSON;
            $dir = Yii::getAlias('@frontend') . '/web/images/'. mb_strtolower($post['ImageManager']['class']). '/';
            if(!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            $resultLink = Yii::$app->urlManagerFrontend->baseUrl.'/images/'. mb_strtolower($post['ImageManager']['class']). '/';
            $file = UploadedFile::getInstanceByName('ImageManager[imageFiles]');

            $model = new ImageManager();
            $model->name =  uniqid() . '.' . $file->extension;
            $model->load($post);
            $model->validate();
            if ($model->hasErrors()) {
                $result = [
                    'error' => $model->getFirstError('file'),
                ];
            } else {
                if (file_exists($dir . $model->name)) {
                    return [
                        'error' => 'ERROR_FILE_ALREADY_EXIST',
                    ];
                }

                if ($file->saveAs($dir . $model->name) and $model->save()) {
                    $result = ['id' => $model->name, 'filelink' => $resultLink . $model->name, 'filename' => $model->name];
                } else {
                    $result = [
                        'error' => 'ERROR_CAN_NOT_UPLOAD_FILE'
                    ];
                }
            }

            return $result;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }

    public function actionDeleteImage()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $id = Yii::$app->request->post('key');

            $model = (new ImageManager())->findOne($id );

            if ($model === null) {
                return ['error' => 'ERROR_FILE_IDENTIFIER_MUST_BE_PROVIDED'];
            }

            $file = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR .'web' . DIRECTORY_SEPARATOR .'images' . DIRECTORY_SEPARATOR . mb_strtolower($model->class) . DIRECTORY_SEPARATOR . $model->name;
            $url = Yii::$app->urlManagerFrontend->baseUrl.'imagesgit ' . DIRECTORY_SEPARATOR . mb_strtolower($model->class) . DIRECTORY_SEPARATOR . $model->name;
            $model->delete();
            if (!file_exists($file)) {
                return ['error' => 'ERROR_FILE_DOES_NOT_EXIST'];
            }

            if (!unlink($file)) {

                return ['error' => 'ERROR_CANNOT_REMOVE_FILE'];
            }
            $result = ['url' => $url, 'id' => $model->name,];
            $model->delete();
            return $result;
        } else {
            throw new BadRequestHttpException('Only DELETE AJAX request is allowed');
        }


    }
    /**
     * File Upload
     *
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionFileUpload()
    {
        $this->enableCsrfValidation = false;
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $dir = Yii::getAlias('@frontend') . '/web/files/';
            if(!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            $resultLink = Yii::$app->urlManagerFrontend->baseUrl.'/files/';
            $file = UploadedFile::getInstanceByName('file');
            $model = new DynamicModel(['file' => $file]);
            $model->addRule('file', 'file', ['extensions' => 'zip, pdf, rar'])->validate();
            if ($model->hasErrors()) {
                $result = [
                    'error' => $model->getFirstError('file'),
                ];
            } else {
                if (file_exists($dir . $model->file->name)) {
                    return [
                        'error' => Yii::t('vova07/imperavi', 'ERROR_FILE_ALREADY_EXIST'),
                    ];
                }
                $model->file->name = uniqid() . '.' . $model->file->extension;
                if ($model->file->saveAs($dir . $model->file->name)) {
                    $result = ['id' => $model->file->name, 'filelink' => $resultLink . $model->file->name];

                } else {
                    $result = [
                        'error' => Yii::t('vova07/imperavi', 'ERROR_CAN_NOT_UPLOAD_FILE'),
                    ];
                }
            }
            return $result;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }
}
