<?php

namespace backend\controllers;

use common\models\PickupPoint;
use common\models\RegionCity;
use common\models\TimePoint;
use Yii;
use common\models\Combaine;
use backend\models\CombaineSearch;
use yii\helpers\BaseJson;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Region;
use common\models\Excursion;
use common\models\TypeCost;
use yii\helpers\ArrayHelper;


/**
 * CombaineController implements the CRUD actions for Combaine model.
 */
class CombaineController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public $layout = '/adminlteLayouts/main';
    /**
     * Lists all Combaine models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CombaineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excursion =(new Excursion())->excursionList;
        $region = (new Region())->regionList;
        $typeCost =(new TypeCost())->typeCostList;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excursion' => $excursion,
            'region' =>  $region,
            'typeCost' =>  $typeCost
        ]);
    }

    /**
     * Displays a single Combaine model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Combaine model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Combaine();
        $excursion = (new Excursion())->excursionList;
        $region = (new Region())->regionList;
        $typeCost = (new TypeCost())->typeCostList;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'excursion' => $excursion,
            'region' =>  $region,
            'typeCost' =>  $typeCost
        ]);
    }

    /**
     * Updates an existing Combaine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $excursion =(new Excursion())->excursionList;
        $region = (new Region())->regionList;
        $typeCost =(new TypeCost())->typeCostList;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'excursion' => $excursion,
            'region' =>  $region,
            'typeCost' =>  $typeCost
        ]);
    }

    /**
     * Deletes an existing Combaine model.
     * If deletion is sucessful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Combaine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Combaine the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Combaine::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Print cities region list
     *
     * @param int $id
     * @return array
     */
    public function actionCitiesRegionListAjax($id = null)
    {
        if($id != null){

            $cities = (new RegionCity())->getCityListAjax($id);
            echo $cities;
            die();
        }

    }

    /**
     * @param null $id
     * @return \yii\web\Response
     *
     */
    public function actionPointTime($id = null)
    {
        $cities = (new RegionCity())->getCityList($id);
        $points = (new PickupPoint())->getCityPointList(ArrayHelper::getColumn($cities,'id'));
        echo  BaseJson::encode( $points );
        die();

    }

    /**
     * Print cities region list
     *
     * @param int $id
     * @return array
     */
    public function actionPointTimeListAjax($id = null)
    {
        if($id != null){

            $points = (new TimePoint())->getTimeListAjax($id);
            echo Json::encode($points);
            die();
        }


    }

}
