<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace backend\controllers;


use dektrium\rbac\controllers\PermissionController as BasePermissionController;

use yii;


/**
 * AdminController allows you to administrate users.
 *
 * @property Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class PermissionController extends BasePermissionController
{
    public $layout = '/adminlteLayouts/main';
}
