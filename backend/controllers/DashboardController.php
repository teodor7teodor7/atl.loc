<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use dektrium\user\filters\AccessRule;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;


class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index','profile','logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public $layout = '/adminlteLayouts/main';

    public function actionIndex()
    {
//        if (!\Yii::$app->user->can('dashboard/index')) {
//            throw new ForbiddenHttpException('Access denied');
//        }

        return $this->render('index');
    }

    public function actionProfile()
    {
//        if (!\Yii::$app->user->can('dashboard/profile')) {
//            throw new ForbiddenHttpException('Access denied');
//        }
        return $this->render('profile');
    }

}
