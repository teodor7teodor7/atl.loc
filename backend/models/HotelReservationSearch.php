<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HotelReservation;

/**
 * HotelReservationSearch represents the model behind the search form of `common\models\HotelReservation`.
 */
class HotelReservationSearch extends HotelReservation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'number_adults', 'number_children', 'date_arrival', 'date_departure', 'type_food', 'created_at', 'updated_at'], 'integer'],
            [['type_customer', 'name_agent', 'name_agency', 'childrens_age', 'email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HotelReservation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number_adults' => $this->number_adults,
            'number_children' => $this->number_children,
            'date_arrival' => $this->date_arrival,
            'date_departure' => $this->date_departure,
            'type_food' => $this->type_food,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type_customer', $this->type_customer])
            ->andFilterWhere(['like', 'name_agent', $this->name_agent])
            ->andFilterWhere(['like', 'name_agency', $this->name_agency])
            ->andFilterWhere(['like', 'childrens_age', $this->childrens_age])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
