<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Feedback;

/**
 * FeedbackSearch represents the model behind the search form of `common\models\Feedback`.
 */
class FeedbackSearch extends Feedback
{

    public $topicFeedback;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'topic_id', 'date', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'email', 'text', 'city', 'exursion', 'answer', 'topicFeedback'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Feedback::find();

        // add conditions that should always apply here
        $query->joinWith(['topicFeedback']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['topicFeedback'] = [
            'asc' => ['topic_feedback.title' => SORT_ASC],
            'desc' => ['topic_feedback.title' => SORT_DESC],
        ];
        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'topic_id' => $this->topic_id,
            'date' => $this->date,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ])->orderBy(['id' => SORT_DESC]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'exursion', $this->exursion])
            ->andFilterWhere(['like', 'answer', $this->answer]);

        $query->joinWith(['topicFeedback' => function ($q) {
            $q->where('topic_feedback.title LIKE "%' . $this->topicFeedback . '%"');
        }]);
        return $dataProvider;
    }
}
