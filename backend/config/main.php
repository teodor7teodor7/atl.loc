<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //    'enableStrictParsing' => true,
            'rules' => [
                'site/signup' => '/user/registration/register',
                'site/signin' => '/user/security/login',
                '/' => 'site/index',
                '<controller>/<action>' => '<controller>/<action>',
            ]
        ],
        'urlManagerFrontend' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'http://atl.loc',
        ],
    ],
    'modules' => [
        'user' => [
            // following line will restrict access to profile, recovery, registration and settings controllers from backend
           'as backend' => 'dektrium\user\filters\BackendFilter',
            'controllerMap' => [
                'admin' => 'backend\controllers\AdminController',
            ],
        ],
        'user' => [
            'class'  => 'dektrium\user\Module',
            'admins' => ['admin'],
            'controllerMap' => [
                'admin' => 'backend\controllers\AdminController',
            ],

        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

    ],
    'params' => $params,
    'language' => 'en_Us',
];
