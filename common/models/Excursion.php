<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\models\ImageManager;
use common\models\Direction;
use common\models\TypeExcursion;
use common\models\ExcursionDirection;
/**
 * This is the model class for table "excursion".
 *
 * @property int $id
 * @property string $title
 */
class Excursion extends \yii\db\ActiveRecord
{
    public $imageFiles;

    public $direction;
    public $typeExcursion;
    public $attraction;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excursion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['title','url'], 'string', 'max' => 255],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['direction','attraction','typeExcursion'], 'each', 'rule' => ['integer']],
            [['title'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() { return date('U'); // unix timestamp
                },
            ],
        ];
    }
    /**
    *Generate exursion list
    */
    public static function getExcursionList() {
        $excursion = Excursion::find()
                ->select(['id', 'title'])
                ->all();
        return ArrayHelper::map($excursion, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImgList()
    {
        return $this->hasMany(ImageManager::className(), ['item_id' => 'id'])->andWhere(['class' => self::tableName()]);
    }

    /**
     * @return array
     */
    public function getImgLinkList()
    {
        $result = [];
        $data = $this->imgList;

        foreach ($data as $one){
            $result[] = Yii::$app->urlManagerFrontend->baseUrl . '/images/excursion/'. $one->name;

        }
        return $result;
    }

    /**
     * @return array
     */
    public function getImgConfigList()
    {
        $result = [];
        $result = ArrayHelper::toArray($this->imgList, [
            ImageManager::className() => [
                'key' => 'id',
                'caption' => 'name',

            ],
        ]);

        return $result;
    }

    /**
     * @return array
     */
    public static function getDirectionList()
    {
        $data = Direction::find()
            ->select(['id', 'title'])
            ->orderBy('title')
            ->all();

        return ArrayHelper::map($data, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getTypeList()
    {
        $data = TypeExcursion::find()
            ->select(['id', 'title'])
            ->orderBy('title')
            ->all();

        return ArrayHelper::map($data, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getAttractionList()
    {
        $data = Attraction::find()
            ->select(['id', 'title'])
            ->orderBy('title')
            ->all();

        return ArrayHelper::map($data, 'id', 'title');
    }

    /**
     * @return array
     */
    public  function getAttractionSelectedList()
    {
        $data = ExcursionAttraction::find()
            ->select(['attraction_id'])
            ->where(['excursion_id' => $this->id])
            ->orderBy('attraction_id')
            ->all();

        $data =  ArrayHelper::getColumn($data, 'attraction_id');

        $data = Attraction::find()
            ->where(['id' => $data])
            ->select(['id', 'title'])
            ->orderBy('title')
            ->all();

        return ArrayHelper::getColumn($data, 'id');
    }

    /**
     * @return array
     */
    public  function getDirectionSelectedList()
    {
        $data = ExcursionDirection::find()
            ->select(['direction_id'])
            ->where(['excursion_id' => $this->id])
            ->orderBy('direction_id')
            ->all();

        $data =  ArrayHelper::getColumn($data, 'direction_id');

        $data = Direction::find()
            ->where(['id' => $data])
            ->select(['id', 'title'])
            ->orderBy('title')
            ->all();

        return ArrayHelper::getColumn($data, 'id');
    }
    /**
     * @return array
     */
    public  function getTypeExcursionSelectedList()
    {
        $data = ExcursionTypeExcursio::find()
            ->select(['type_excursion_id'])
            ->where(['excursion_id' => $this->id])
            ->orderBy('type_excursion_id')
            ->all();

        $data =  ArrayHelper::getColumn($data, 'type_excursion_id');

        $data = TypeExcursion::find()
            ->where(['id' => $data])
            ->select(['id', 'title'])
            ->orderBy('title')
            ->all();

        return ArrayHelper::getColumn($data, 'id');
    }
    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        ImageManager::imageManagerUpload($this);

        ExcursionDirection::deleteAll(['excursion_id' => $this->id]);
        if(!empty($this->direction)){
            $array = [];
            foreach ($this->direction as $one) {

                $array[] = [$this->id, (int)$one];
            }
            Yii::$app->db->createCommand()->batchInsert('excursion_direction', ['excursion_id', 'direction_id'], $array)->execute();

        }

        ExcursionTypeExcursio::deleteAll(['excursion_id' => $this->id]);
        if(!empty($this->typeExcursion)){
            $array = [];
            foreach ($this->typeExcursion as $one) {

                $array[] = [$this->id, (int)$one];
            }
            Yii::$app->db->createCommand()->batchInsert('excursion_type_excursion', ['excursion_id', 'type_excursion_id'], $array)->execute();

        }

        ExcursionAttraction::deleteAll(['excursion_id' => $this->id]);

        if(!empty($this->attraction)){
            $array = [];
            foreach ($this->attraction as $one) {

                $array[] = [$this->id, (int)$one];
            }
            Yii::$app->db->createCommand()->batchInsert('excursion_attraction', ['excursion_id', 'attraction_id'], $array)->execute();

        }
    }

    public function afterDelete()
    {
        ImageManager::imagesDelete($this);
        ImageManager::deleteAll(['item_id' => $this->id, 'class' => 'Excursion']);
        ExcursionDirection::deleteAll(['excursion_id' => $this->id]);
        ExcursionTypeExcursio::deleteAll(['excursion_id' => $this->id]);
        ExcursionAttraction::deleteAll(['excursion_id' => $this->id]);

        return parent::afterDelete();
    }
}
