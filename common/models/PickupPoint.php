<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pickup_point".
 *
 * @property int $id
 * @property string $title
 * @property int $child_switch
 */
class PickupPoint extends \yii\db\ActiveRecord
{

    const CITY_SWITCH_ON = 1;
    const CITY_SWITCH_OFF = 0;

    public $childPoint;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pickup_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['child_switch', 'city_id'], 'integer'],
            [ ['childPoint'], 'each', 'rule' => ['integer']],
            [['title'], 'string', 'max' => 255],
            ['child_switch',  'default', 'value' => PickupPoint::CITY_SWITCH_OFF]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'child_switch' => Yii::t('app', 'Child Switch'),
        ];
    }

    /**
     * @return array
     */
    public function getChildPointList()
    {
        $data = ChildPoint::find()->where(['parent_id' => null])
        ->orWhere(['parent_id' => $this->id ])->all();
        return ArrayHelper::map($data,'id', 'title');


    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Yii::$app->db->createCommand()->update('child_point', ['parent_id' => null ], ['parent_id' => $this->id])->execute();

        if(!empty($this->childPoint)){
            $array = [];

            foreach ($this->childPoint as $one) {

                $array[] = (int)$one;
            }
            Yii::$app->db->createCommand()->update('child_point', ['parent_id' => $this->id ], ['id' => $array])->execute();

        }

    }

    /**
     *
     */
    public function afterDelete()
    {
        Yii::$app->db->createCommand()->update('child_point', ['parent_id' => null ], ['parent_id' => $this->id])->execute();

        return parent::afterDelete();
    }

    /**
     * @return array
     */
    public function getChildPointSelect()
    {
        if($this->id) {
            $data = ChildPoint::find()->where(['parent_id' => $this->id])->all();
            return ArrayHelper::getColumn($data, 'id');
        }

        return null;
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (empty($this->childPoint) or $this->child_switch == Combaine::CITY_SWITCH_OFF) {

                $this->child_switch = Combaine::CITY_SWITCH_OFF;
            }
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getCityPointList($cities = null)
    {
        $result = [];
        $data = PickupPoint::find()->where(['city_id' => $cities])->all();
        $result =  ArrayHelper::map($data,'id', 'title', 'city_id');
        return $result;
    }
}
