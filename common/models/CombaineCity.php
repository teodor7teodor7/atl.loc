<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "excursion_city".
 *
 * @property int $excursion_id
 * @property int $city_id
 */
class CombaineCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'combaine_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['combaine_id', 'city_id'], 'required'],
            [['combaine_id', 'city_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'excursion_id' => Yii::t('app', 'Excursion ID'),
            'city_id' => Yii::t('app', 'City ID'),
        ];
    }

}
