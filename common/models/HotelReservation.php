<?php

namespace common\models;

use common\behaviors\DateToTimeBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "hotel_reservation".
 *
 * @property int $id
 * @property string $type_customer
 * @property string $name_agent
 * @property string $name_agency
 * @property int $number_adults
 * @property int $number_children
 * @property int $date_arrival
 * @property int $date_departure
 * @property int $type_food
 * @property string $childrens_age
 * @property string $email
 * @property string $phone
 * @property int $created_at
 * @property int $updated_at
 */
class HotelReservation extends \yii\db\ActiveRecord
{

    const TYPE_CUSTOMER_PERSON = 0;
    const TYPE_CUSTOMER_AGENT = 1;

    const TYPE_FOOD_BREAKFAST = 0;
    const TYPE_FOOD_BREAKFAST_AND_DINNER = 1;
    const TYPE_FOOD_ALL_INCLUSIVE = 2;

    public $dateArrivalRaw;
    public $dateDepartureRaw;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_reservation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_customer', 'type_food', 'email', 'phone'], 'required'],
            ['email','email'],
            [['number_adults', 'number_children', 'date_arrival', 'date_departure', 'type_food', 'created_at', 'updated_at'], 'integer'],
            [['type_customer', 'name_agent', 'name_agency', 'childrens_age', 'email', 'phone'], 'string', 'max' => 255],
            [[ 'dateArrivalRaw', 'dateDepartureRaw'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_customer' => Yii::t('app', 'Type Customer'),
            'name_agent' => Yii::t('app', 'Name Agent'),
            'name_agency' => Yii::t('app', 'Name Agency'),
            'number_adults' => Yii::t('app', 'Number Adults'),
            'number_children' => Yii::t('app', 'Number Children'),
            'date_arrival' => Yii::t('app', 'Date Arrival'),
            'date_departure' => Yii::t('app', 'Date Departure'),
            'type_food' => Yii::t('app', 'Type Food'),
            'childrens_age' => Yii::t('app', 'Childrens Age'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypeCustomer()
    {
        return [
            self::TYPE_CUSTOMER_PERSON => 'Person',
            self::TYPE_CUSTOMER_AGENT => 'Agent',
        ];
    }
    /**
     * @return array
     */
    public static function getTypeFood()
    {
        return [
            self::TYPE_FOOD_BREAKFAST => 'Breakfast',
            self::TYPE_FOOD_BREAKFAST_AND_DINNER => 'Breakfast and dinner',
            self::TYPE_FOOD_ALL_INCLUSIVE => 'All inclusive',
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() { return date('U'); // unix timestamp
                },
            ],
            [
                    'class' => DateToTimeBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_VALIDATE => 'dateArrivalRaw',
                        ActiveRecord::EVENT_AFTER_FIND => 'dateArrivalRaw',
                    ],
                    'timeAttribute' => 'date_arrival',
            ],
            [
                    'class' => DateToTimeBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_VALIDATE => 'dateDepartureRaw',
                        ActiveRecord::EVENT_AFTER_FIND => 'dateDepartureRaw',
                    ],
                    'timeAttribute' => 'date_departure',
            ],
        ];

    }

}
