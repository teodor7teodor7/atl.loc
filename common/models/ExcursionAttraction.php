<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "excursion_attraction".
 *
 * @property int $excursion_id
 * @property int $attraction_id
 */
class ExcursionAttraction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excursion_attraction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['excursion_id', 'attraction_id'], 'required'],
            [['excursion_id', 'attraction_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'excursion_id' => Yii::t('app', 'Excursion ID'),
            'attraction_id' => Yii::t('app', 'Attraction ID'),
        ];
    }
}
