<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\TourCategory;
use yii\web\NotFoundHttpException;



/**
 * This is the model class for table "tour".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property int $category_id
 * @property string $url
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TourCategory $category
 */
class Tour extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tour';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'category_id', 'url'], 'required'],
            [['text'], 'string'],
            [['category_id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TourCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'category_id' => Yii::t('app', 'Category ID'),
            'url' => Yii::t('app', 'Url'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TourCategory::className(), ['id' => 'category_id']);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() { return date('U'); // unix timestamp
                },
            ],
        ];
    }
    /**
     * Get tour list.
     *
     * @return array
     */
    public function getTourList($url = null)
    {
        $data = [];
        $model = (new TourCategory())->find()->where(['url' => $url])->one();
        if($model){
            $data = Tour::find()->where(['category_id' => $model->id])->all();
        }
        return $data;
    }
}
