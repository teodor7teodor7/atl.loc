<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\HotelReting;
use yii\helpers\ArrayHelper;
use common\models\ImageManager;
use common\models\HotelCity;
/**
 * This is the model class for table "hotel".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $url
 * @property int $reting_id
 * @property int $reting_type
 * @property int $status
 * @property string $images_carousel
 * @property int $created_at
 * @property int $updated_at
 */
class Hotel extends \yii\db\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    const TYPE_POINTS = 0;
    const TYPE_STARS = 1;

    public $imageFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'reting_id', 'reting_type', 'city_id'], 'required'],
            [['text'], 'string'],
            [['city_id', 'reting_id', 'reting_type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
           // [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 10]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
            'url' => 'Url',
            'reting_id' => 'Reting ID',
            'reting_type' => 'Reting Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function () {
                    return date('U'); // unix timestamp
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_OFF => 'Off',
            self::STATUS_ON => 'On',
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_POINTS => 'Points',
            self::TYPE_STARS => 'Stars',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelReting()
    {
        return $this->hasOne(HotelReting::className(), ['id' => 'reting_id']);
    }

    public static function getHotelRetingList()
    {
        $data = HotelReting::find()
            ->select(['id', 'title'])
            ->orderBy('title')
            ->all();

        return ArrayHelper::map($data, 'id', 'title');
    }
    public static function getHotelCityList()
    {
        $data = HotelCity::find()
            ->select(['id', 'name'])
            ->orderBy('name')
            ->all();

        return ArrayHelper::map($data, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelImgList()
    {
        return $this->hasMany(ImageManager::className(), ['item_id' => 'id'])->andWhere(['class' => self::tableName()]);
    }

    /**
     * @return array
     */
    public function getHotelImgLinkList()
    {
        $result = [];
        $data = $this->hotelImgList;

        foreach ($data as $one){
            $result[] = Yii::$app->urlManagerFrontend->baseUrl . '/images/hotel/'. $one->name;

        }
        return $result;
    }

    /**
     * @return array
     */
    public function getHotelImgConfigList()
    {
        $result = [];
        $result = ArrayHelper::toArray($this->hotelImgList, [
            ImageManager::className() => [
                'key' => 'id',
                'caption' => 'name',

            ],
        ]);

        return $result;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

            ImageManager::imageManagerUpload($this);
    }

    /**
     *
     */
    public function afterDelete()
    {
        ImageManager::imagesDelete($this);
    }

}
