<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "image_manager".
 *
 * @property int $id
 * @property string $name
 * @property string $class
 * @property int $item_id
 * @property int $created_at
 * @property int $updated_at
 */
class ImageManager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image_manager';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'class', 'item_id'], 'required'],
            [['item_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'class'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'class' => Yii::t('app', 'Class'),
            'item_id' => Yii::t('app', 'Item ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function () {
                    return date('U'); // unix timestamp
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public static function imageManagerUpload($dataModel)
    {

        $dataImage = UploadedFile::getInstances($dataModel, 'imageFiles');
        if (!empty($dataImage)) {
            foreach ($dataImage as $file) {
                $dir = Yii::getAlias('@frontend') . '/web/images/' . mb_strtolower($dataModel->formName()) . '/';
                if (!file_exists($dir)) {
                    FileHelper::createDirectory($dir);

                }
                $resultLink = Yii::$app->urlManagerFrontend->baseUrl . '/images/' . mb_strtolower($dataModel->formName()) . '/';
                $model = new ImageManager();
                $model->name = uniqid() . '.' . $file->extension;
                $model->item_id = $dataModel->id;
                $model->class = $dataModel->formName();
                $model->load($dataModel);
                $model->validate();

                if ($model->hasErrors()) {
                    $result = [
                        'error' => $model->getFirstError('file'),
                    ];
                } else {
                    if (file_exists($dir . $model->name)) {
                        return [
                            'error' => 'ERROR_FILE_ALREADY_EXIST',
                        ];
                    }

                    if ($file->saveAs($dir . $model->name) and $model->save()) {
                        $result = ['id' => $model->name, 'filelink' => $resultLink . $model->name, 'filename' => $model->name];
                    } else {
                        $result = [
                            'error' => 'ERROR_CAN_NOT_UPLOAD_FILE'
                        ];
                    }
                }
            }
            return $result;
        }
        return null;
    }

    public static function imageDelete($model)
    {
        if ($model === null) {
            return ['error' => 'ERROR_FILE_IDENTIFIER_MUST_BE_PROVIDED'];
        }

        $file = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . mb_strtolower($model->class) . DIRECTORY_SEPARATOR . $model->name;
        $url = Yii::$app->urlManagerFrontend->baseUrl . 'images' . DIRECTORY_SEPARATOR . mb_strtolower($model->class) . DIRECTORY_SEPARATOR . $model->name;
        $model->delete();
        if (!file_exists($file)) {
            return ['error' => 'ERROR_FILE_DOES_NOT_EXIST'];
        }

        if (!unlink($file)) {

            return ['error' => 'ERROR_CANNOT_REMOVE_FILE'];
        }
        $result = ['url' => $url, 'id' => $model->name,];
        $model->delete();
        return $result;
    }
    public static function imagesDelete($model)
    {
        $images = ImageManager::find()->where((['item_id' => $model->id]))->all();
        foreach ($images as $image) {
                self::imageDelete($image);
        }
        ImageManager::deleteAll(['item_id' => $model->id]);

    }


}
