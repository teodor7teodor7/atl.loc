<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "time_point".
 *
 * @property int $point_id
 * @property int $day
 * @property int $combaine_id
 * @property int $time
 */
class TimePoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'time_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id', 'day', 'combaine_id', 'time'], 'required'],
            [['point_id', 'day', 'combaine_id', 'time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'point_id' => Yii::t('app', 'Point ID'),
            'day' => Yii::t('app', 'Day'),
            'combaine_id' => Yii::t('app', 'Combaine ID'),
            'time' => Yii::t('app', 'Time'),
        ];
    }


    /**
     * @return array
     */
    public function getTimeListAjax($id)
    {
        $result = [];
        $data = TimePoint::find()->where(['combaine_id' => $id])->all();
        $result =  ArrayHelper::map($data,'day', 'time' , 'point_id');
        return $result;
    }
}
