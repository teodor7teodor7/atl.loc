<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "price".
 *
 * @property int $id
 * @property int $typecost_id
 * @property int $excursion_id
 * @property string $price
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['typecost_id', 'excursion_id'], 'required'],
            [['typecost_id', 'excursion_id'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'typecost_id' => Yii::t('app', 'Typecost ID'),
            'excursion_id' => Yii::t('app', 'Excursion ID'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

}
