<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseJson;

/**
 * This is the model class for table "region_city".
 *
 * @property int $id
 * @property int $region_id
 * @property int $city_id
 */
class RegionCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'city_id'], 'required'],
            [['region_id', 'city_id'], 'integer'],
            [['region_id', 'city_id'], 'unique', 'targetAttribute' => ['region_id', 'city_id'], 'message'=>'Dublicate value'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'region_id' => 'Region ID',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCityListAjax($id)
    {
        $data = RegionCity::find()->where(['region_id' => $id])->all();
        $cityList = City::find()->where(['id' => $data])->all();

        $data = [];
        if (!empty($cityList)) {
            foreach ($cityList as $key => $value) {
                $data['results'][] = ['id' => $value['id'], 'text' => $value['title']];
            }

            return BaseJson::encode($data);
        }
    }

    /**
     * @inheritdoc
     */
    public function getCityList($id)
    {
        $data = RegionCity::find()->where(['region_id' => $id])->all();
        $data =  ArrayHelper::getColumn( $data,'id');
        $cityList = City::find()->where(['id' => $data])->all();
        return  $cityList;
    }

}
