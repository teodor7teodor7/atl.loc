<?php

namespace common\models;

use Yii;

use common\behaviors\DateToTimeBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property int $topic_id
 * @property string $name
 * @property string $email
 * @property string $text
 * @property string $city
 * @property string $exursion
 * @property int $date
 * @property string $answer
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Feedback extends \yii\db\ActiveRecord
{

    public $dateRaw;
    const STATUS_OFF = 0;
    const STATUS_ON =  1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_id', 'name', 'email', 'city', 'exursion', 'dateRaw', 'text'], 'required'],
            [['topic_id', 'status', 'created_at', 'updated_at','date'], 'integer'],
            [['text', 'answer'], 'string'],
            [['dateRaw'], 'date', 'format' => 'php:d.m.Y'],
            ['email','email'],
            [['name', 'email', 'city', 'exursion'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'topic_id' => Yii::t('app', 'Topic'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'text' => Yii::t('app', 'Text'),
            'city' => Yii::t('app', 'City'),
            'exursion' => Yii::t('app', 'Exursion'),
            'date' => Yii::t('app', 'Date'),
            'answer' => Yii::t('app', 'Answer'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => DateToTimeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_VALIDATE => 'dateRaw',
                    ActiveRecord::EVENT_AFTER_FIND => 'dateRaw',
                ],
                'timeAttribute' => 'date', //Атрибут модели в котором хранится время в int
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() { return date('U'); // unix timestamp
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses() {
        return [
            self::STATUS_OFF => 'Off',
            self::STATUS_ON => 'On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopicFeedback()
    {
        return $this->hasOne(TopicFeedback::className(), ['id' => 'topic_id']);
    }
}
