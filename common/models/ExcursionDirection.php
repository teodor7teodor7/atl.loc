<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "excursion_direction".
 *
 * @property int $excursion_id
 * @property int $direction_id
 */
class ExcursionDirection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excursion_direction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['excursion_id', 'direction_id'], 'required'],
            [['excursion_id', 'direction_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'excursion_id' => Yii::t('app', 'Excursion ID'),
            'direction_id' => Yii::t('app', 'Direction ID'),
        ];
    }
}
