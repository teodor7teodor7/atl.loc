<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "attraction".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $url
 * @property int $created_at
 * @property int $updated_at
 */
class Attraction extends \yii\db\ActiveRecord
{

    public $imageFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attraction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'url'], 'required'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
//            ['imageFiles', 'image', 'extensions' => 'jpg, gif, png',
//                'skipOnEmpty' => false],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'image' => Yii::t('app', 'Image'),
            'url' => Yii::t('app', 'Url'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() { return date('U'); // unix timestamp
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasMany(ImageManager::className(), ['item_id' => 'id'])->andWhere(['class' => self::tableName()]);
    }
    /**
     * @return array
     */
    public function getImageLink()
    {
        $result = [];
        $data = $this->image;

        foreach ($data as $one){
            $result[] = Yii::$app->urlManagerFrontend->baseUrl . '/images/attraction/'. $one->name;

        }
        return $result;
    }
    /**
     * @return array
     */
    public function getImageConfig()
    {
        $result = [];
        $result = ArrayHelper::toArray($this->image, [
            ImageManager::className() => [
                'key' => 'id',
                'caption' => 'name',

            ],
        ]);

        return $result;
    }
    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        ImageManager::imageManagerUpload($this);
    }

    public function afterDelete()
    {
        ImageManager::imagesDelete($this);

        return parent::afterDelete();
    }
}
