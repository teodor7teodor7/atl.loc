<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "excursion_type_excursion".
 *
 * @property int $excursion_id
 * @property int $type_excursion_id
 */
class ExcursionTypeExcursio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excursion_type_excursion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['excursion_id', 'type_excursion_id'], 'required'],
            [['excursion_id', 'type_excursion_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'excursion_id' => Yii::t('app', 'Excursion ID'),
            'type_excursion_id' => Yii::t('app', 'Type Excursion ID'),
        ];
    }
}
