<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\CombaineCity;

use DateTime;

/**
 * This is the model class for table "combaine".
 *
 * @property int $id
 * @property int $excursion_id
 * @property int $region_id
 * @property int $typecost_id
 * @property string $price
 */
class Combaine extends \yii\db\ActiveRecord
{
   public $price;
   public $city;
   public $date;
   public $exception;
   public $period;
   public $days;
   public $time;
   public $daysTime;

   const CITY_SWITCH_ON = 1;
   const CITY_SWITCH_OFF = 0;

   const DAY_SUNDAY = 1;
   const DAY_MONDAY = 2;
   const DAY_TUESDAY = 3;
   const DAY_WEDNESDAY = 4;
   const DAY_THURSDAY = 5;
   const DAY_FRIDAY = 6;
   const DAY_SATURDAY = 7;


   const PERIOD_WEEK = 1;
   const PERIOD_MONTH = 2;
   const PERIOD_YEAR = 3;

   const TYPE_EXCURSION = 0;
   const TYPE_EXCEPTION = 1;
   const TYPE_DAY = 2;

        /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'combaine';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['excursion_id', 'region_id', 'price', 'date_start'], 'required'],
            [['excursion_id', 'region_id', 'period_type', 'period_data'], 'integer'],
            [['excursion_id', 'region_id', ], 'unique', 'targetAttribute' => ['excursion_id', 'region_id'], 'message'=>'Dublicate value'],
            [['created_at', 'updated_at'], 'integer'],
            [['price'], 'each', 'rule' => ['number']],
            [['city'], 'each', 'rule' => ['integer']],
            ['city_switch',  'default', 'value' => Combaine::CITY_SWITCH_OFF],
            [['days'], 'each', 'rule' => ['integer']],
            [['date_start'], 'date', 'format' => 'dd.mm.yyyy'],
            [['time'], 'safe'],
            [['daysTime'], 'safe'],
            [['date', 'exception'], 'validateDate', 'skipOnEmpty'=> false],

        ];
    }
    public function validateDate()
    {

        if (!empty($this->date)) {
            $date = explode(',', $this->date);

            foreach ($date as $one) {
                $params = explode('/', $one);
                if (checkdate($params[0], $params[1], $params[2]) == false) {
                    $this->addError('date', 'Error Date');
                    return false;
                }
            }
        }
        if (!empty($this->excaption)) {
            $date = explode(',', $this->excaption);

            foreach ($date as $one) {
                $params = explode('/', $one);
                if (checkdate($params[0], $params[1], $params[2]) == false) {
                    $this->addError('date', 'Error Exception');
                    return false;
                }
            }
        }
            return true;

    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'excursion_id' => 'Excursion ID',
            'region_id' => 'Region ID',
            'typecost_id' => 'Typecost ID',
            'price' => 'Price',
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() { return date('U'); // unix timestamp
                },
            ],
        ];
    }
    /**
     * @return array
     */
    public static function getDaysWeek()
    {
        return [
            self::DAY_SUNDAY => 'Sunday',
            self::DAY_MONDAY => 'Monday',
            self::DAY_TUESDAY => 'Tuesday',
            self::DAY_WEDNESDAY => 'Wednesday',
            self::DAY_THURSDAY => 'Thursday',
            self::DAY_FRIDAY => 'Friday',
            self::DAY_SATURDAY => 'Saturday',
        ];
    }
    /**
     * @return array
     */
    public static function getPeriodType()
    {
        return [
            self::PERIOD_WEEK => 'Week',
            self::PERIOD_MONTH => 'Month',
            self::PERIOD_YEAR => 'Year',
        ];
    }

    /**
     * @return array
     */
    public static function getPeriodData()
    {
        return [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4'
        ];
    }
    /**
     * @return array
     */
    public function getExcursionPrices()
    {
        $data =[];

        if (isset($this->excursion_id)) {
            $data = (new Price())::find()->where(['excursion_id' => $this->excursion_id])->all();
        }

        return ArrayHelper::map( $data, 'typecost_id', 'price');
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function getExcursionCityList()
    {
        $data = CombaineCity::find()->where(['combaine_id' => $this->id])->all();
        $data = ArrayHelper::getColumn($data, 'city_id');
        return $data;

    }

    /**
     * @return array
     */
    public function getExcursionDaysSelect()
    {
        $data = ExcursionDate::find()
            ->where(['combaine_id' => $this->id])
            ->andWhere(['type' => self::TYPE_DAY])->all();

        return ArrayHelper::getColumn($data,'data');

    }

    /**
     * @return array
     */
    public function getExcursionDateSelect()
    {
        $data = ExcursionDate::find()
            ->where(['combaine_id' => $this->id])
            ->andWhere(['type' => self::TYPE_EXCURSION])->all();

        $data = ArrayHelper::getColumn($data,'data');


        $date = '';
        foreach ($data as  $one){

            $date .=  date('m/d/Y', $one).',';
        }

        return trim($date, ",");

    }
    /**
     * @return array
     */
    public function getExcursionExceptionSelect()
    {
        $data = ExcursionDate::find()
            ->where(['combaine_id' => $this->id])
            ->andWhere(['type' => self::TYPE_EXCEPTION])->all();

        $data = ArrayHelper::getColumn($data,'data');
        $date = '';
        foreach ($data as  $one){

            $date .=  date('m/d/Y', $one).',';
        }
        return trim($date, ",");

    }
    /**
     * @return array
     */
    public function getCityList()
    {
        $data = RegionCity::find()->where(['region_id' => $this->region_id])->all();
        $data =  ArrayHelper::getColumn($data,'city_id');
        $cityList = City::find()->where(['id' => $data])->all();
        return ArrayHelper::map($cityList,'id', 'title');

    }
    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        parent::afterSave($insert, $changedAttributes);

        $prices = [];
        Price::deleteAll(['id' => $this->excursion_id]);

        foreach ($this->price as $key => $one){
            $prices[] = [$key, $this->excursion_id, $one];
        }

        Yii::$app->db->createCommand()->batchInsert('price', ['typecost_id', 'excursion_id', 'price'], $prices)->execute();

        CombaineCity::deleteAll(['combaine_id' => $this->id]);

        if ($this->city_switch == Combaine::CITY_SWITCH_ON) {
            foreach ($this->city as $key => $one){
                $cities[] = [ $this->id, $one];
            }
            Yii::$app->db->createCommand()->batchInsert('combaine_city', [ 'combaine_id', 'city_id'],  $cities)->execute();
        }

        ExcursionDate::deleteAll(['combaine_id' => $this->id]);

        if (!empty($this->date)) {

            $date = explode(',', $this->date);


            foreach ($date as $one){
                $data = new ExcursionDate();
                $data->data = (new DateTime($one))->getTimestamp();
                $data->type = self::TYPE_EXCURSION;
                $data->combaine_id = $this->id;
                $data->save();

            }

        }

        if (!empty($this->exception)) {

            $date = explode(',', $this->exception);

            foreach ($date as $one){
                $data = new ExcursionDate();
                $data->data = (new DateTime($one))->getTimestamp();
                $data->type = self::TYPE_EXCEPTION;
                $data->combaine_id = $this->id;
                $data->save();

            }
        }

        if (!empty($this->days)) {
            $data = [];
            foreach ($this->days as $one){
                $data[] = [$one, self::TYPE_DAY, $this->id];
            }
            Yii::$app->db->createCommand()->batchInsert('excursion_date', [ 'data', 'type', 'combaine_id'],  $data)->execute();
        }
        TimePoint::deleteAll(['combaine_id' => $this->id]);
        if(!empty($this->daysTime)){
            foreach ($this->daysTime as $keyPoints => $days){
                $timePoints = [];
                foreach ($days as $keyDays => $time){

                    $timePoints []= [$keyPoints, $keyDays, $this->id, $time ];
                }
                Yii::$app->db->createCommand()->batchInsert('time_point', [ 'point_id', 'day', 'combaine_id', 'time'],  $timePoints)->execute();

            }

        }

    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (empty($this->city) or $this->city_switch == Combaine::CITY_SWITCH_OFF) {

                $this->city_switch = Combaine::CITY_SWITCH_OFF;
                CombaineCity::deleteAll(['combaine_id' => $this->id]);
            }

            $this->date_start = (new DateTime($this->date_start))->getTimestamp();

            return true;
        }
        return false;
    }
    public function afterDelete()
    {
        CombaineCity::deleteAll(['combaine_id' => $this->id]);
        TimePoint::deleteAll(['combaine_id' => $this->id]);
    }



}
