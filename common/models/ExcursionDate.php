<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "excursion_date".
 *
 * @property int $id
 * @property int $data
 * @property int $type
 * @property int $excursion_id
 */
class ExcursionDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excursion_date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'type', 'combaine_id'], 'required'],
            [['data', 'type', 'combaine_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data' => Yii::t('app', 'Data'),
            'type' => Yii::t('app', 'Type'),
            'excursion_id' => Yii::t('app', 'Excursion ID'),
        ];
    }
}
