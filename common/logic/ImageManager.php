<?php
/**
 * Created by Vyacheslav Bodrov.
 * Date: 19.01.2018
 * Time: 10:35
 */

namespace common\logic;


use Yii;
use yii\helpers\BaseFileHelper;
use yii\imagine\Image;
use frontend\models\UploadAvatarForm;
use yii\web\UploadedFile;


class UploadAvatar
{
    const SIZE = 128;
    const ROTATE = 90;
    const MODE = 0655;
    private $modelImages;

    public function __construct($modelImages) {
        $this->modelImages = $modelImages;
    }

    public function upload(){
        $imageName = uniqid(UploadAvatarForm::FILE_PREFIX);
        $this->modelImages->file = UploadedFile::getInstance($this->modelImages, 'file');
        $this->modelImages->file_path = UploadAvatarForm::FILE_PATH . Yii::$app->user->identity->id .'/avatar/';
        $this->modelImages->file_name = $imageName. '.' . $this->modelImages->file->extension;
        BaseFileHelper::createDirectory($this->modelImages->file_path, self::MODE , true );
        $this->modelImages->file->saveAs( $this->modelImages->file_path . DIRECTORY_SEPARATOR.$this->modelImages->file_name);
        Image::thumbnail($this->modelImages->file_path . DIRECTORY_SEPARATOR.$this->modelImages->file_name, self::SIZE, self::SIZE)
            ->save($this->modelImages->file_path . DIRECTORY_SEPARATOR.$this->modelImages->file_name);

        return $this->modelImages->save();
    }
}
